#!/bin/bash

MYDIR="$(cd "`dirname "$0"`";pwd)"


cd ${HOME}
tar xf "${MYDIR}/vim.tar.gz"
mv vim .vim

cd ${MYDIR}
cp bashrc "${HOME}/.bashrc"
cp my_functions "${HOME}/.my_functions"
cp vimrc "${HOME}/.vimrc"
cp profile "${HOME}/.profile"
cp tmux.conf "${HOME}/.tmux.conf"
cp -r bin "${HOME}/"

