set nocompatible
set backspace=indent,eol,start

set backup
set backupdir=~/.vim/backup
set directory=~/.vim/tmp
set undofile
set undodir=~/.vim/undodir

set history=50
set ruler
set showcmd
set incsearch
map Q gq
noremap ;2u <nop> " prevent shift+enter which sends ;2u from undoing 2 edits w/ wsltty
noremap ;5u <nop> " ctrl+enter

if has('mouse')
  set mouse=a
endif

syntax on
set hlsearch

set number
set tabstop=4
set softtabstop=4
set shiftwidth=4
"set expandtab


filetype plugin indent on
augroup vimrcEx
	au!
	autocmd FileType text setlocal textwidth=78
	autocmd BufReadPost *
		\ if line("'\"") > 1 && line("'\"") <= line("$") |
		\   exe "normal! g`\"" |
		\ endif
augroup END
set ai

let &termencoding=&encoding
set encoding=utf-8
set fileencodings=utf-8,gbk,ucs-bom,cp936
set fileformats=unix,dos

"execute pathogen#infect()
"execute pathogen#helptags()

set wildmenu
set showmatch

let mapleader = ","
let g:mapleader = ","

set wildignore=*.o,*.~,*.pyc,*.class
set lazyredraw

set laststatus=2
set statusline=%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l:%c\ --%p%%--

set ignorecase
set smartcase

augroup filetypedetect
	au! BufRead,BufNewFile *nc set ft=nc
augroup END


set background=dark
colorscheme solarized

set mouse=c

autocmd FileType tex :set tw=70

let &colorcolumn=81
highlight ColorColumn ctermbg=6 ctermfg=0

